import React from 'react';

const IndividualLine = props => {
    return (
        <div className="individualLine">
            <input type="text" className="personName" placeholder="Name" onChange={props.onPersonChange} value={props.person}/>
            <label className="price"><input type="text" className="personsPrice" onChange={props.onPriceClick} value={props.price}/> сом</label>
            <button type="button" className="delete" onClick={props.remove}>X</button>
        </div>
    );
};

export default IndividualLine;