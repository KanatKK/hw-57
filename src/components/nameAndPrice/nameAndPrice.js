import React from 'react';

const NameAndPrice = props => {
    return (
        <>
            <p>{props.names}: {props.prices}</p>
        </>
    );
};

export default NameAndPrice;