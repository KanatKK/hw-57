import React, {useState} from 'react';
import './App.css';
import IndividualLine from "../../components/individualLine/individualLine";
import NameAndPrice from "../../components/nameAndPrice/nameAndPrice";

const App = () => {
    const [value, setValue] = useState('');
    const selectValue = e => {
        if (e.target.checked) {
            setValue(e.target.value)
        }
    };

    let allDisplay = {display: 'none'};
    let individualDisplay = {display: 'none'};
    let totalPriceForAllDisplay = {display: 'none'};
    let totalPriceForIndividualDisplay = {display: 'none'};

    if (value === 'all') {
        individualDisplay.display = 'none'
        allDisplay.display = 'block'
    }
    if (value === 'individual') {
        allDisplay.display = 'none'
        individualDisplay.display = 'block'
    }

    const [all, setAll] = useState([{people: '', sum: '', tips: '', delivery: ''}]);

    const [allPrices, setAllPrices] = useState([{total: '', allPeople: '', amountForEach: '', display: false}]);

    const changePeople = event => {
        const allCopy = [...all];
        allCopy[0].people = event.target.value;
        setAll(allCopy)
    };

    const changeSum = event => {
        const allCopy = [...all];
        allCopy[0].sum = event.target.value;
        setAll(allCopy)
    };

    const changeTips = event => {
        const allCopy = [...all];
        allCopy[0].tips = event.target.value;
        setAll(allCopy);
    };

    const changeDelivery = event => {
        const allCopy = [...all];
        allCopy[0].delivery = event.target.value;
        setAll(allCopy);
    };

    const getPriceAll = () => {
        const allPricesCopy = [...allPrices];
        allPricesCopy[0].display = true
        const tips = Number(all[0].sum) / 100 * Number(all[0].tips);
        const totalSum = Number(all[0].sum) + Number(tips) + Number(all[0].delivery);
        const amountForEach = Number(totalSum) / Number(all[0].people);
        allPricesCopy[0].total = Math.ceil(totalSum);
        allPricesCopy[0].allPeople = all[0].people;
        allPricesCopy[0].amountForEach = Math.ceil(amountForEach);
        setAllPrices(allPricesCopy);
    };

    if (allPrices[0].display) {
        totalPriceForAllDisplay.display = 'block';
    }

    const [individual, setIndividual] = useState([{person: '', price: ''},]);
    const [individualPrices, setIndividualPrices] = useState([{tips:'', delivery:'', total:'', display: false}]);

    const addTips = event => {
        const individualPricesCopy = [...individualPrices];
        individualPricesCopy[0].tips = event.target.value
        setIndividualPrices(individualPricesCopy)
    }

    const addDelivery = event => {
        const individualPricesCopy = [...individualPrices];
        individualPricesCopy[0].delivery = event.target.value
        setIndividualPrices(individualPricesCopy)
    }

    const addPerson = (event, index) => {
        const individualCopy = [...individual];
        individualCopy[index].person = event.target.value
        setIndividual(individualCopy);
    };

    const addPrice = (event, index) => {
        const individualCopy = [...individual];
        individualCopy[index].price = event.target.value
        setIndividual(individualCopy);
    };

    const addLine = () => {
        const individualCopy = [...individual];
        individualCopy.push({person: '', price: ''},);
        setIndividual(individualCopy);
    };

    const removeLine = index => {
        const individualCopy = [...individual];
        individualCopy.splice(index, 1);
        setIndividual(individualCopy);
    }

    let individualLineList = individual.map((individual, index) => {
        return (
            <IndividualLine key={index} onPersonChange = {event => addPerson(event, index)}
                            onPriceClick = {event => addPrice(event, index)}
                            remove = {() => removeLine(index)}
                            person = {individual.person} price = {individual.price} />);
    });

        const prices = []

        const getPriceIndividual = () => {
            const individualPricesCopy = [...individualPrices];
            individualPricesCopy[0].display = true
            const reducer = (accumulator, currentValue) => accumulator + currentValue;
            const individualDelivery = Math.ceil(Number(individualPricesCopy[0].delivery) / Number(individual.length));
            for (const key in individual) {
                console.log(Number(individual[key].price) + Number(individual[key].price) / 100 * individualPricesCopy[0].tips + individualDelivery)
                prices.push(Math.ceil(Number(individual[key].price) + Number(individual[key].price) / 100 * individualPricesCopy[0].tips + individualDelivery))
            }
            const sum = prices.reduce(reducer)
            individualPricesCopy[0].total = Number(sum)
            setIndividualPrices(individualPricesCopy)
            for (let i = 0; i < prices.length; i++) {
                const individualCopy = [...individual];
                individualCopy[i].price = prices[i]
                setIndividual(individualCopy)
            }
        };


    if(individualPrices[0].display) {
        totalPriceForIndividualDisplay.display = "block"
    }


    let nameAndPrice = individual.map((individual, index) => {
        return (
            <NameAndPrice key={index} names={individual.person} prices={individual.price} />)
    });

    return (
      <div className="container">
          <div className="calc">
              <h5 className="calcHeading">Сумма заказа считается:</h5>
              <label className="changers"><input type="radio" name="changer" value="all" id="allChanger" onChange={selectValue}/> Поровну между всеми участниками.</label>
              <label className="changers"><input type="radio" name="changer" value="individual" id="individualChanger" onChange={selectValue}/> Каждому индивидуально.</label>
              <div className="all" style={allDisplay}>
                <div className="allLine">
                    <p className="allTxt">Человек:</p>
                    <label className="allArea"><input type="text" className="people" defaultValue="0" onChange={changePeople}/> чел.</label>
                </div>
                  <div className="allLine">
                      <p className="allTxt">Сумма заказа:</p>
                      <label className="allArea"><input type="text" className="sum" defaultValue="0" onChange={changeSum}/> сом</label>
                  </div>
                  <div className="allLine">
                      <p className="allTxt">Процент чаевых:</p>
                      <label className="allArea"><input type="text" className="allTips" defaultValue="0" onChange={changeTips}/> %</label>
                  </div>
                  <div className="allLine">
                      <p className="allTxt">Доставка:</p>
                      <label className="allArea"><input type="text" className="allDelivery" defaultValue="0" onChange={changeDelivery}/> сом</label>
                  </div>
                  <button type="button" className="priceForAll" onClick={getPriceAll}>Расчитать</button>
                  <div className="totalPriceForAll" style={totalPriceForAllDisplay}>
                      <p>Общая сумма: {allPrices[0].total} сом</p>
                      <p>Количество человек: {allPrices[0].allPeople}</p>
                      <p>Каждый платит по: {allPrices[0].amountForEach} сом</p>
                  </div>
              </div>
              {/*//////////////////////????//////////////////////////////???????///////////////////////////////???????/////////////*/}
              <div className="individual" style={individualDisplay}>
                  {individualLineList}
                  <button type="button" className="add" onClick={addLine}>+</button>
                  <label className="individualTips">Процент чаевых: <input type="text" defaultValue="0" onChange={addTips}/> %</label>
                  <label className="delivery">Доставка: <input type="text" defaultValue="0" onChange={addDelivery}/> сом</label>
                  <button type="button" className="priceForIndividual" onClick={getPriceIndividual}>Расчитать</button>
                  <div className="totalPriceForIndividual" style={totalPriceForIndividualDisplay}>
                      <p>Общая сумма: {individualPrices[0].total}</p>
                      {nameAndPrice}
                  </div>
              </div>
          </div>
      </div>
  );
};

export default App;
